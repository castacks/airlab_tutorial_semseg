{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as pl\n",
    "\n",
    "import torch\n",
    "from torch.autograd import Variable\n",
    "from torch.optim import SGD\n",
    "from torch import nn\n",
    "import torch.nn.functional as F\n",
    "from torchvision.datasets import MNIST\n",
    "from torchvision.transforms import ToTensor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, a quick look at the MNIST dataset. We have already included it in the repo -- but remember to unzip it first. (``gunzip ./mnist/processed/*.gz``)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist = MNIST('./mnist', download=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following should display numbers and their labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist[0][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist[0][1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist[20][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist[20][1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pytorch doesn't understand PIL image objects -- we need to transform them to tensors. This is done with the ``transform`` object. Other preprocessing steps could be added as transforms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mnist_train = MNIST('./mnist', train=True, transform=ToTensor())\n",
    "mnist_test = MNIST('./mnist', train=False, transform=ToTensor())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the nifty `DataLoader` provided by Pytorch to help iterate through the dataset. Among other things it can load data in parallel threads, which is useful, but we won't do that here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_loader = torch.utils.data.DataLoader(mnist_train, batch_size=8, shuffle=True)\n",
    "test_loader = torch.utils.data.DataLoader(mnist_test, batch_size=1, shuffle=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we define our model. There's nothing too special about this model, some convolutions, pooling and finally fully connected layers. \n",
    "\n",
    "Note that some steps in ``forward`` use the `functional` version, e.g. ``F.relu()``. This is less verbose for modules and useful for modules with no parameters. \n",
    "\n",
    "Note the use of ``log_softmax``: this should be used along with the `nll_loss` insted of an actual `log` and `softmax` with cross-entropy, for numerical stability."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Net(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.conv1 = nn.Conv2d(1, 32, 5, padding=2)\n",
    "        self.conv2 = nn.Conv2d(32, 64, 5, padding=2)                \n",
    "        self.fc1 = nn.Linear(64*7*7, 1024) # annoyingly we have to figure out the size\n",
    "        self.fc2 = nn.Linear(1024, 10) # 10 digits\n",
    "        \n",
    "    def forward(self, x):\n",
    "        conv1out = F.relu(self.conv1(x))\n",
    "        pool1out = F.max_pool2d(conv1out, 2)\n",
    "        conv2out = F.relu(self.conv2(pool1out))\n",
    "        pool2out = F.max_pool2d(conv2out, 2)\n",
    "        pool2flat = pool2out.view(-1, 64*7*7) # flatten\n",
    "        fc1out = F.relu(self.fc1(pool2flat))\n",
    "        fc2out = F.dropout(fc1out, training=self.training)\n",
    "        fc1out = self.fc2(fc2out)\n",
    "        logscore = F.log_softmax(fc1out, 1)\n",
    "        # we'll save some outputs for future reference\n",
    "        self.conv1out = conv1out.data\n",
    "        self.pool1out = pool1out.data\n",
    "        self.fc1out = fc1out.data\n",
    "        self.logscore = logscore.data\n",
    "        \n",
    "        return logscore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Net()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pytorch has various useful gradient optimizers, including SGD, RMSProp, Adam, Adamax, etc. They have different ways of tweaking the gradient information for improved convergence rates and robustness (in theory). Vanilla SGD with momentum is a decent default choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optim = SGD(net.parameters(), lr=0.001, momentum=0.9)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# feel free to stop around i=5000\n",
    "net.train()\n",
    "losses = []\n",
    "for epoch in range(10):    \n",
    "    for i, (xt, yt) in enumerate(train_loader):\n",
    "        # load tensors into autograd variables\n",
    "        x, y = Variable(xt), Variable(yt)\n",
    "        # zero out gradients\n",
    "        optim.zero_grad()\n",
    "        # predict a (log softmax) distribution\n",
    "        pred = net(x)\n",
    "        # compute loss, NLL\n",
    "        loss = F.nll_loss(pred, y)\n",
    "        # compute gradients\n",
    "        loss.backward()\n",
    "        losses.append(loss.data[0])\n",
    "        # perform gradient update step\n",
    "        optim.step()        \n",
    "        if (i % 100) == 0:\n",
    "            amax = pred.data.max(1)[1]\n",
    "            acc = amax.eq(y.data).sum()/float(y.size(0))\n",
    "            print 'itr', i, 'loss', losses[-1], 'accuracy', acc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A quick look at the losses over time. The noisiness is not unusual. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.plot(range(len(losses)), losses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can evaluate the model on the test data. Should be >95% accuracy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net.eval()\n",
    "correct = 0\n",
    "for xt, yt in test_loader:\n",
    "    x, y = Variable(xt), Variable(yt)\n",
    "    pred = net(x)\n",
    "    amax = pred.data.max(1)[1]\n",
    "    correct += amax.eq(y.data).sum()\n",
    "print 'Accuracy:', float(correct)/len(mnist_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pytorch makes saving our model quite easy: ``torch.save('mnist_net.pth', net.state_dict())``.\n",
    "To load, ``net.load_state_dict(torch.load('mnist_net.pth'))``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's useful to examine the parameters and intermediate outputs of the network. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x.size()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net.conv1out.size()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net.conv1.weight.size()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = net.conv1.weight.data.numpy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.imshow(w[0, 0, :, :]); pl.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f1 = net.conv1out.numpy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f1.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.imshow(f1[0, 0, :, :])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
