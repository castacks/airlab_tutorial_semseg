from PIL import Image
from collections import OrderedDict
import json

import numpy as np
from spath import Path

import torch
import torch.utils.data

from imgutils import remapping


MEAN_PIXEL = np.asarray((102.71886444,
                         115.77290344,
                         123.51186371), dtype='f4').reshape((3, 1, 1))

OUTSIZE = (128, 128)


def preproc_rgb(img, to_bgr):
    img = np.asarray(img).copy()
    img = img.astype('f4')
    if to_bgr:
        img = img[:, :, ::-1]
    # to bc01
    img = np.rollaxis(img, 2)
    img -= MEAN_PIXEL
    return img.copy()


def preproc_labels(lbl, mapping):
    lbl = np.asarray(lbl).copy()
    lbl2 = remapping.rgb_image_to_label_image(lbl, mapping).astype('i8')
    # return lbl2[None, ...].copy()
    return lbl2.copy()


class MsrcDataset(torch.utils.data.Dataset):
    def __init__(self,
                 base_dir,
                 instance_list_fname='splits.json',
                 split_key='train'):
        self.base_dir = Path(base_dir)
        if (self.base_dir/'metadata.json').exists():
            self.metadata = (self.base_dir/'metadata.json').read_json()
            self.rgb_label_mapping = {}
            self.label_name_mapping = {}
            for rec in self.metadata:
                rgb = (rec['r'], rec['g'], rec['b'])
                self.rgb_label_mapping[rgb] = rec['label']
                self.label_name_mapping[rec['label']] = rec['name']
            self.label_rgb_mapping = {v:k for (k,v) in self.rgb_label_mapping.items()}

        self.instance_list_fname = Path(instance_list_fname)
        self.split_key = split_key
        splits = (self.base_dir/self.instance_list_fname).read_json()
        self.idirs = splits[split_key]

    def get_instance(self, index, preprocess=False):
        idir = self.idirs[index]
        blob = OrderedDict()
        rgb_fname = self.base_dir/idir/'rgb.jpg'
        labels_fname = self.base_dir/idir/'labels.png'

        img = Image.open(rgb_fname)
        lbl = Image.open(labels_fname)
        # very crude resizing
        img = img.resize(OUTSIZE, resample=Image.BILINEAR)
        lbl = lbl.resize(OUTSIZE, resample=Image.NEAREST)

        if preprocess:
            blob['rgb'] = preproc_rgb(img, to_bgr=True)
            blob['labels'] = preproc_labels(lbl, self.rgb_label_mapping)
        else:
            blob['rgb'] = img
            blob['labels'] = lbl
        return blob

    def __getitem__(self, index):
        blob = self.get_instance(index, preprocess=True)
        return blob

    def __len__(self):
        return len(self.idirs)
